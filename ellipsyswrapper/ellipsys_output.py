
import os.path
import numpy as np

class EllipSys1DOutput(object):
    """
    Reads output files from EllipSys1D.
    """

    def __init__(self, project):

        self.project = project

        self.multi = False  # Flag that indicates whether output files
                            # are of the format grid.A1??.?force
                            # or grid.?force
        self.clear()

    def clear(self):
        self.x = []  # x coordinates of geometry')
        self.y = []  # y coordinates of geometry')


    def read(self):
        """ 
        Read <casename>.points 
        """

        self.clear()

        # read the grid.points file
        var_map = {}
        filename = self.project + '.points'
        if os.path.exists(filename):
            fid = open(filename, 'r')
            fid.readline()
            fid.readline()
            fid.readline()
            line = fid.readline()
            ivar = 0
            while line.startswith('#'):
                name = line.split()[-1]
                var_map[ivar] = name
                if not hasattr(self, 'pt_ext_%s' % name):
                    setattr(self, 'pt_ext_%s' % name, [])
                pos = fid.tell()
                line = fid.readline()
                ivar += 1
            # unroll readline so all data is read by loadtxt
            fid.seek(pos, 0)
            data = np.loadtxt(fid)
            for j in range(data.shape[1]):
                var = getattr(self, 'pt_ext_%s' % var_map[j])
                var.append(data[:, j])
            fid.close()


class EllipSys2DOutput(object):
    """
    Reads output files from EllipSys2D.
    """

    def __init__(self, project):

        self.project = project

        self.multi = False  # Flag that indicates whether output files
                            # are of the format grid.A1??.?force
                            # or grid.?force
        self.clear()

    def clear(self):

        self.nstep_forces = []  # Iteration counter history
        self.nstep_res = []  #
        self.nstep_ave = 500  #
        self.time = []
        self.res_u = []
        self.res_v = []
        self.res_w = []
        self.res_p = []
        self.res_te = []  # Turbulent kinetic energy residual history')
        self.res_omega = []  # Specific dissipation residual history')
        self.res_gamma = [] # Intermittency residual history')
        self.res_retheta = []  # Re momentum thickness residual history')
        self.fvx = []  # x-direction viscous force history')
        self.fvy = []  # y-direction viscous force history')
        self.fx = []  # x-direction viscous+pressure force history')
        self.fy = []  # y-direction viscous+pressure force history')
        self.mz = []  # Moment (around x/c=0.25) history')
        self.fvx_ave = []  # Averaged x-direction viscous force')
        self.fvy_ave = []  # Averaged y-direction viscous force')
        self.fx_ave = []  # Averaged x-direction viscous+pressure force')
        self.fy_ave = []  # Averaged y-direction viscous+pressure force')
        self.mz_ave = []  # Averaged moment (around x/c=0.25)')
        self.fvx_j = []  # x-direction viscous force history')
        self.fvy_j = []  # y-direction viscous force history')
        self.fx_j = []  # x-direction viscous+pressure force history')
        self.fy_j = []  # y-direction viscous+pressure force history')
        self.mz_j = []  # Moment (around x/c=0.25) history')
        self.fvx_ave_j = []  # Averaged x-direction viscous force')
        self.fvy_ave_j = []  # Averaged y-direction viscous force')
        self.fx_ave_j = []  # Averaged x-direction viscous+pressure force')
        self.fy_ave_j = []  # Averaged y-direction viscous+pressure force')
        self.mz_ave_j = []  # Averaged moment (around x/c=0.25)')
        self.x = []  # x coordinates of geometry')
        self.y = []  # y coordinates of geometry')
        self.x_j = []  # x coordinates of geometry')
        self.y_j = []  # y coordinates of geometry')
        self.pressure = []  # Pressure distribution')
        self.pressure_ave = []  # Pressure distribution')
        self.skin_friction = []  # Skin friction distribution')
        self.skin_friction_ave = []  # Skin friction distribution')
        self.gamma = []
        self.gamma_ave = []
        self.pressure_j = []  # Pressure distribution')
        self.pressure_ave_j = []  # Pressure distribution')
        self.skin_friction_j = []  # Skin friction distribution')
        self.skin_friction_ave_j = []  # Skin friction distribution')
        self.gamma_j = []
        self.gamma_ave_j = []
        self.nsurf = 10  # number of wall surfaces in domain')


    def read(self):
        """ Read <casename>.?force,<casename>.?pr and <casename>.res """

        self.clear()

        # first test whether force and pressure files have the format grid.?force or grid.A10?.?force
        for grlev in range(1,6):
            filename = self.project + '.A101.' + str(grlev) + 'force'
            if os.path.exists(filename):
                self.multi = True
                break

        # attempt to read <self.project>.A1??.?force
        # forces are saved into a nested list as e.g. self.fy[grlev][surf]
        nlev=0
        for grlev in range(1, 6):
            if self.multi:
                filename = self.project + '.A101.' + str(grlev) + 'force'
            else:
                filename = self.project + '.' + str(grlev) + 'force'
            if os.path.exists(filename):
                if self.multi:
                    self.nstep_forces.append([])
                    self.time.append([])
                    self.fvx.append([])
                    self.fvy.append([])
                    self.fx.append([])
                    self.fy.append([])
                    self.mz.append([])
                    self.fvx_ave.append([])
                    self.fvy_ave.append([])
                    self.fx_ave.append([])
                    self.fy_ave.append([])
                    self.mz_ave.append([])
                    self.nstep_forces.append([])
                    self.time.append([])
                    self.fvx_j.append([])
                    self.fvy_j.append([])
                    self.fx_j.append([])
                    self.fy_j.append([])
                    self.mz_j.append([])
                    self.fvx_ave_j.append([])
                    self.fvy_ave_j.append([])
                    self.fx_ave_j.append([])
                    self.fy_ave_j.append([])
                    self.mz_ave_j.append([])
                # we need to loop over multiple surfaces
                    for surf in range(1,self.nsurf):
                        filename = self.project + '.A1' + ('%02d'%surf) + '.' + str(grlev) + 'force'
                        if os.path.exists(filename):
                            try:
                                grid_force = np.loadtxt(filename)
                                self.nstep_forces[nlev].append(grid_force[:, 0])
                                ave = -min(grid_force[:, 0].shape[0], self.nstep_ave)
                                self.time[nlev].append(grid_force[:, 1])
                                if grid_force.shape[1] == 7:
                                    self.fvx[nlev].append(grid_force[:, 2])
                                    self.fvy[nlev].append(grid_force[:, 3])
                                    self.fx[nlev].append(grid_force[:, 4])
                                    self.fy[nlev].append(grid_force[:, 5])
                                    self.mz[nlev].append(grid_force[:, 6])
                                    self.fvx_ave[nlev].append(np.mean(grid_force[ave:, 2]))
                                    self.fvy_ave[nlev].append(np.mean(grid_force[ave:, 3]))
                                    self.fx_ave[nlev].append(np.mean(grid_force[ave:, 4]))
                                    self.fy_ave[nlev].append(np.mean(grid_force[ave:, 5]))
                                    self.mz_ave[nlev].append(np.mean(grid_force[ave:, 6]))
                                elif grid_force.shape[1] == 12:  # with complex number outputs
                                    self.fvx[nlev].append(grid_force[:, 2])
                                    self.fvy[nlev].append(grid_force[:, 4])
                                    self.fx[nlev].append(grid_force[:, 6])
                                    self.fy[nlev].append(grid_force[:, 8])
                                    self.mz[nlev].append(grid_force[:, 10])
                                    self.fvx_ave[nlev].append(np.mean(grid_force[ave:, 2]))
                                    self.fvy_ave[nlev].append(np.mean(grid_force[ave:, 4]))
                                    self.fx_ave[nlev].append(np.mean(grid_force[ave:, 6]))
                                    self.fy_ave[nlev].append(np.mean(grid_force[ave:, 8]))
                                    self.mz_ave[nlev].append(np.mean(grid_force[ave:, 10]))
                                    # complex part
                                    self.fvx_j[nlev].append(grid_force[:, 3])
                                    self.fvy_j[nlev].append(grid_force[:, 5])
                                    self.fx_j[nlev].append(grid_force[:, 7])
                                    self.fy_j[nlev].append(grid_force[:, 9])
                                    self.mz_j[nlev].append(grid_force[:, 11])
                                    self.fvx_ave_j[nlev].append(np.mean(grid_force[ave:, 3]))
                                    self.fvy_ave_j[nlev].append(np.mean(grid_force[ave:, 5]))
                                    self.fx_ave_j[nlev].append(np.mean(grid_force[ave:, 7]))
                                    self.fy_ave_j[nlev].append(np.mean(grid_force[ave:, 9]))
                                    self.mz_ave_j[nlev].append(np.mean(grid_force[ave:, 11]))
                            except:
                                print('Error on reading of', filename)

                else:
                # there's only one surface
                    if os.path.exists(filename):
                        try:
                            grid_force = np.loadtxt(filename)
                            self.nstep_forces.append(grid_force[:, 0])
                            self.time.append(grid_force[:, 1])
                            ave = -min(grid_force[:, 0].shape[0], self.nstep_ave)
                            if grid_force.shape[1] == 7:  # with complex number outputs
                                self.fvx.append(grid_force[:, 2])
                                self.fvy.append(grid_force[:, 3])
                                self.fx.append(grid_force[:, 4])
                                self.fy.append(grid_force[:, 5])
                                self.fvx_ave.append(np.mean(grid_force[ave:, 2]))
                                self.fvy_ave.append(np.mean(grid_force[ave:, 3]))
                                self.fx_ave.append(np.mean(grid_force[ave:, 4]))
                                self.fy_ave.append(np.mean(grid_force[ave:, 5]))
                                self.mz_ave.append(np.mean(grid_force[ave:, 6]))
                            elif grid_force.shape[1] == 12:  # with complex number outputs
                                self.fvx.append(grid_force[:, 2])
                                self.fvy.append(grid_force[:, 4])
                                self.fx.append(grid_force[:, 6])
                                self.fy.append(grid_force[:, 8])
                                self.mz.append(grid_force[:, 10])
                                self.fvx_ave.append(np.mean(grid_force[ave:, 2]))
                                self.fvy_ave.append(np.mean(grid_force[ave:, 4]))
                                self.fx_ave.append(np.mean(grid_force[ave:, 6]))
                                self.fy_ave.append(np.mean(grid_force[ave:, 8]))
                                self.mz_ave.append(np.mean(grid_force[ave:, 10]))
                                self.fvx_j.append(grid_force[:, 3])
                                self.fvy_j.append(grid_force[:, 5])
                                self.fx_j.append(grid_force[:, 7])
                                self.fy_j.append(grid_force[:, 9])
                                self.mz_j.append(grid_force[:, 11])
                                self.fvx_ave_j.append(np.mean(grid_force[ave:, 3]))
                                self.fvy_ave_j.append(np.mean(grid_force[ave:, 5]))
                                self.fx_ave_j.append(np.mean(grid_force[ave:, 7]))
                                self.fy_ave_j.append(np.mean(grid_force[ave:, 9]))
                                self.mz_ave_j.append(np.mean(grid_force[ave:, 11]))
                        except:
                            print('Error on reading of', filename)
                nlev += 1

        # read the pressure distributions
        # we need to loop over multiple surfaces
        nlev = 0
        for grlev in range(1, 6):
            if self.multi:
                filename = self.project + '.A101.' + str(grlev) + 'pr'
            else:
                filename = self.project + '.' + str(grlev) + 'pr'
            if os.path.exists(filename):
                if self.multi:
                    self.x.append([])
                    self.y.append([])
                    self.pressure.append([])
                    self.skin_friction.append([])
                    self.pressure_ave.append([])
                    self.skin_friction_ave.append([])
                    self.gamma.append([])
                    self.gamma_ave.append([])
                    self.x_j.append([])
                    self.y_j.append([])
                    self.pressure_j.append([])
                    self.skin_friction_j.append([])
                    self.pressure_ave_j.append([])
                    self.skin_friction_ave_j.append([])
                    self.gamma_j.append([])
                    self.gamma_ave_j.append([])
                    for surf in range(1, self.nsurf):
                        filename = self.project + '.A1' + ('%02d'%surf) + '.' + str(grlev) + 'pr'
                        if os.path.exists(filename):
                            grid_pr = np.loadtxt(filename)
                            if grid_pr.shape[1] == 8:
                                self.x[nlev].append(grid_pr[:, 0])
                                self.y[nlev].append(grid_pr[:, 1])
                                self.pressure[nlev].append(grid_pr[:, 2])
                                self.skin_friction[nlev].append(grid_pr[:, 3])
                                self.pressure_ave[nlev].append(grid_pr[:, 4])
                                self.skin_friction_ave[nlev].append(grid_pr[:, 5])
                                self.gamma[nlev].append(grid_pr[:, 6])
                                self.gamma_ave[nlev].append(grid_pr[:, 7])
                            elif grid_pr.shape[1] == 16:
                                self.x[nlev].append(grid_pr[:, 0])
                                self.y[nlev].append(grid_pr[:, 2])
                                self.pressure[nlev].append(grid_pr[:, 4])
                                self.skin_friction[nlev].append(grid_pr[:, 6])
                                self.pressure_ave[nlev].append(grid_pr[:, 8])
                                self.skin_friction_ave[nlev].append(grid_pr[:, 10])
                                self.gamma[nlev].append(grid_pr[:, 12])
                                self.gamma_ave[nlev].append(grid_pr[:, 14])
                                self.x_j[nlev].append(grid_pr[:, 1])
                                self.y_j[nlev].append(grid_pr[:, 3])
                                self.pressure_j[nlev].append(grid_pr[:, 5])
                                self.skin_friction_j[nlev].append(grid_pr[:, 7])
                                self.pressure_ave_j[nlev].append(grid_pr[:, 9])
                                self.skin_friction_ave_j[nlev].append(grid_pr[:, 11])
                                self.gamma_j[nlev].append(grid_pr[:, 13])
                                self.gamma_ave_j[nlev].append(grid_pr[:, 15])
                else:
                # there's only one surface
                    if os.path.exists(filename):
                        grid_pr = np.loadtxt(filename)
                        if grid_pr.shape[1] == 8:
                            self.x.append(grid_pr[:, 0])
                            self.y.append(grid_pr[:, 1])
                            self.pressure.append(grid_pr[:, 2])
                            self.skin_friction.append(grid_pr[:, 3])
                            self.pressure_ave.append(grid_pr[:, 4])
                            self.skin_friction_ave.append(grid_pr[:, 5])
                            self.gamma.append(grid_pr[:, 6])
                            self.gamma_ave.append(grid_pr[:, 7])
                        elif grid_pr.shape[1] == 16:
                            self.x.append(grid_pr[:, 0])
                            self.y.append(grid_pr[:, 2])
                            self.pressure.append(grid_pr[:, 4])
                            self.skin_friction.append(grid_pr[:, 6])
                            self.pressure_ave.append(grid_pr[:, 8])
                            self.skin_friction_ave.append(grid_pr[:, 10])
                            self.gamma.append(grid_pr[:, 12])
                            self.gamma_ave.append(grid_pr[:, 14])
                            self.x_j.append(grid_pr[:, 1])
                            self.y_j.append(grid_pr[:, 3])
                            self.pressure_j.append(grid_pr[:, 5])
                            self.skin_friction_j.append(grid_pr[:, 7])
                            self.pressure_ave_j.append(grid_pr[:, 9])
                            self.skin_friction_ave_j.append(grid_pr[:, 11])
                            self.gamma_j.append(grid_pr[:, 13])
                            self.gamma_ave_j.append(grid_pr[:, 15])
                nlev += 1



class EllipSys3DOutput(object):
    """
    Reads output files from EllipSys3D.
    """

    def __init__(self, project):

        self.project = project

        self.multi = False  # Flag that indicates whether output files
                            # are of the format grid.A1??.?force
                            # or grid.?force
        self.clear()

    def clear(self):

        self.nstep_forces = []  # Iteration counter history
        self.nstep_res = []  #
        self.nstep_ave = 500  #
        self.time = []
        self.res_u = []
        self.res_v = []
        self.res_w = []
        self.res_p = []
        self.res_te = []  # Turbulent kinetic energy residual history')
        self.res_omega = []  # Specific dissipation residual history')
        self.res_gamma = [] # Intermittency residual history')
        self.res_retheta = []  # Re momentum thickness residual history')
        self.fvx = []  # x-direction viscous force history')
        self.fvy = []  # y-direction viscous force history')
        self.fvz = []  # z-direction viscous force history')
        self.fx = []  # x-direction viscous+pressure force history')
        self.fy = []  # y-direction viscous+pressure force history')
        self.fz = []  # y-direction viscous+pressure force history')
        self.mx = []  # Moment (around x/c=0.25) history')
        self.my = []  # Moment (around x/c=0.25) history')
        self.mz = []  # Moment (around x/c=0.25) history')
        self.fvx_ave = []  # Averaged x-direction viscous force')
        self.fvy_ave = []  # Averaged y-direction viscous force')
        self.fvz_ave = []  # Averaged y-direction viscous force')
        self.fx_ave = []  # Averaged x-direction viscous+pressure force')
        self.fy_ave = []  # Averaged y-direction viscous+pressure force')
        self.fz_ave = []  # Averaged y-direction viscous+pressure force')
        self.mx_ave = []  # Averaged moment (around x/c=0.25)')
        self.my_ave = []  # Averaged moment (around x/c=0.25)')
        self.mz_ave = []  # Averaged moment (around x/c=0.25)')
        self.x = []  # x coordinates of geometry')
        self.y = []  # y coordinates of geometry')
        self.z = []  # y coordinates of geometry')
        self.gamma = []
        self.gamma_ave = []
        self.nsurf = 10  # number of wall surfaces in domain')

        self.ext_X_coords = []
        self.ext_X_fx = []
        self.ext_X_fy = []
        self.ext_X_fz = []
        self.ext_Y_coords = []
        self.ext_Y_fx = []
        self.ext_Y_fy = []
        self.ext_Y_fz = []
        self.ext_Z_coords = []
        self.ext_Z_fx = []
        self.ext_Z_fy = []
        self.ext_Z_fz = []
        self.ext_X_fxv = []
        self.ext_X_fyv = []
        self.ext_X_fzv = []
        self.ext_Y_fxv = []
        self.ext_Y_fyv = []
        self.ext_Y_fzv = []
        self.ext_Z_fxv = []
        self.ext_Z_fyv = []
        self.ext_Z_fzv = []

    def read(self):
        """ Read <casename>.?force,<casename>.?pr and <casename>.res """

        self.clear()

        # first test whether force and pressure files have the format grid.?force or grid.A10?.?force
        for grlev in range(1,6):
            filename = self.project + '.A101.' + str(grlev) + 'force'
            if os.path.exists(filename):
                self.multi = True
                break

        # attempt to read <self.project>.A1??.?force
        # forces are saved into a nested list as e.g. self.fy[grlev][surf]
        nlev=0
        for grlev in range(1, 6):
            if self.multi:
                filename = self.project + '.A101.' + str(grlev) + 'force'
            else:
                filename = self.project + '.' + str(grlev) + 'force'
            if os.path.exists(filename):
                if self.multi:
                    self.nstep_forces.append([])
                    self.time.append([])
                    self.fvx.append([])
                    self.fvy.append([])
                    self.fx.append([])
                    self.fy.append([])
                    self.mz.append([])
                    self.fvx_ave.append([])
                    self.fvy_ave.append([])
                    self.fx_ave.append([])
                    self.fy_ave.append([])
                    self.mz_ave.append([])
                # we need to loop over multiple surfaces
                    for surf in range(1,self.nsurf):
                        filename = self.project + '.A1' + ('%02d'%surf) + '.' + str(grlev) + 'force'
                        if os.path.exists(filename):
                            try:
                                grid_force = np.loadtxt(filename)
                                self.nstep_forces[nlev].append(grid_force[:, 0])
                                ave = -min(grid_force[:, 0].shape[0], self.nstep_ave)
                                self.time[nlev].append(grid_force[:, 1])
                                if grid_force.shape[1] == 14: 
                                    self.fvx[nlev].append(grid_force[:, 2])
                                    self.fvy[nlev].append(grid_force[:, 3])
                                    self.fvy[nlev].append(grid_force[:, 4])
                                    self.fx[nlev].append(grid_force[:, 5])
                                    self.fy[nlev].append(grid_force[:, 6])
                                    self.fy[nlev].append(grid_force[:, 7])
                                    self.mx[nlev].append(grid_force[:, 8])
                                    self.my[nlev].append(grid_force[:, 9])
                                    self.mz[nlev].append(grid_force[:, 10])
                                    self.fvx_ave[nlev].append(np.mean(grid_force[ave:, 2]))
                                    self.fvy_ave[nlev].append(np.mean(grid_force[ave:, 3]))
                                    self.fvz_ave[nlev].append(np.mean(grid_force[ave:, 4]))
                                    self.fx_ave[nlev].append(np.mean(grid_force[ave:, 5]))
                                    self.fy_ave[nlev].append(np.mean(grid_force[ave:, 6]))
                                    self.fz_ave[nlev].append(np.mean(grid_force[ave:, 7]))
                                    self.mx_ave[nlev].append(np.mean(grid_force[ave:, 8]))
                                    self.my_ave[nlev].append(np.mean(grid_force[ave:, 9]))
                                    self.mz_ave[nlev].append(np.mean(grid_force[ave:, 10]))
                                elif grid_force.shape[1] == 26:
                                    self.fvx[nlev].append(grid_force[:, 2]+grid_force[:, 3]*1j)
                                    self.fvy[nlev].append(grid_force[:, 4]+grid_force[:, 5]*1j)
                                    self.fvy[nlev].append(grid_force[:, 6]+grid_force[:, 7]*1j)
                                    self.fx[nlev].append(grid_force[:, 8]+grid_force[:, 9]*1j)
                                    self.fy[nlev].append(grid_force[:, 10]+grid_force[:, 11]*1j)
                                    self.fy[nlev].append(grid_force[:, 12]+grid_force[:, 13]*1j)
                                    self.mx[nlev].append(grid_force[:, 14]+grid_force[:, 15]*1j)
                                    self.my[nlev].append(grid_force[:, 16]+grid_force[:, 17]*1j)
                                    self.mz[nlev].append(grid_force[:, 18]+grid_force[:, 19]*1j)
                                    self.fvx_ave[nlev].append(np.mean(grid_force[ave:, 2])
                                                              + np.mean(grid_force[ave:, 3])*1j)
                                    self.fvy_ave[nlev].append(np.mean(grid_force[ave:, 4]) 
                                                              + np.mean(grid_force[ave:, 5])*1j)
                                    self.fvz_ave[nlev].append(np.mean(grid_force[ave:, 6]) 
                                                              + np.mean(grid_force[ave:, 7])*1j)
                                    self.fx_ave[nlev].append(np.mean(grid_force[ave:, 8]) 
                                                              + np.mean(grid_force[ave:, 9])*1j)
                                    self.fy_ave[nlev].append(np.mean(grid_force[ave:, 10]) 
                                                              + np.mean(grid_force[ave:, 11])*1j)
                                    self.fz_ave[nlev].append(np.mean(grid_force[ave:, 12]) 
                                                              + np.mean(grid_force[ave:, 13])*1j)
                                    self.mx_ave[nlev].append(np.mean(grid_force[ave:, 14]) 
                                                              + np.mean(grid_force[ave:, 15])*1j)
                                    self.my_ave[nlev].append(np.mean(grid_force[ave:, 16]) 
                                                              + np.mean(grid_force[ave:, 17])*1j)
                                    self.mz_ave[nlev].append(np.mean(grid_force[ave:, 18]) 
                                                              + np.mean(grid_force[ave:, 19])*1j)
                                    
                            except:
                                print('Error on reading of', filename)

                else:
                # there's only one surface
                    if os.path.exists(filename):
                        try:
                            grid_force = np.loadtxt(filename)
                            self.nstep_forces.append(grid_force[:, 0])
                            ave = -min(grid_force[:, 0].shape[0], self.nstep_ave)
                            self.time.append(grid_force[:, 1])
                            if grid_force.shape[1] == 14: 
                                self.fvx.append(grid_force[:, 2])
                                self.fvy.append(grid_force[:, 3])
                                self.fvy.append(grid_force[:, 4])
                                self.fx.append(grid_force[:, 5])
                                self.fy.append(grid_force[:, 6])
                                self.fy.append(grid_force[:, 7])
                                self.mx.append(grid_force[:, 8])
                                self.my.append(grid_force[:, 9])
                                self.mz.append(grid_force[:, 10])
                                self.fvx_ave.append(np.mean(grid_force[ave:, 2]))
                                self.fvy_ave.append(np.mean(grid_force[ave:, 3]))
                                self.fvz_ave.append(np.mean(grid_force[ave:, 4]))
                                self.fx_ave.append(np.mean(grid_force[ave:, 5]))
                                self.fy_ave.append(np.mean(grid_force[ave:, 6]))
                                self.fz_ave.append(np.mean(grid_force[ave:, 7]))
                                self.mx_ave.append(np.mean(grid_force[ave:, 8]))
                                self.my_ave.append(np.mean(grid_force[ave:, 9]))
                                self.mz_ave.append(np.mean(grid_force[ave:, 10]))
                            elif grid_force.shape[1] == 26:
                                self.fvx.append(grid_force[:, 2]+grid_force[:, 3]*1j)
                                self.fvy.append(grid_force[:, 4]+grid_force[:, 5]*1j)
                                self.fvy.append(grid_force[:, 6]+grid_force[:, 7]*1j)
                                self.fx.append(grid_force[:, 8]+grid_force[:, 9]*1j)
                                self.fy.append(grid_force[:, 10]+grid_force[:, 11]*1j)
                                self.fy.append(grid_force[:, 12]+grid_force[:, 13]*1j)
                                self.mx.append(grid_force[:, 14]+grid_force[:, 15]*1j)
                                self.my.append(grid_force[:, 16]+grid_force[:, 17]*1j)
                                self.mz.append(grid_force[:, 18]+grid_force[:, 19]*1j)
                                self.fvx_ave.append(np.mean(grid_force[ave:, 2])
                                                          + np.mean(grid_force[ave:, 3])*1j)
                                self.fvy_ave.append(np.mean(grid_force[ave:, 4]) 
                                                          + np.mean(grid_force[ave:, 5])*1j)
                                self.fvz_ave.append(np.mean(grid_force[ave:, 6]) 
                                                          + np.mean(grid_force[ave:, 7])*1j)
                                self.fx_ave.append(np.mean(grid_force[ave:, 8]) 
                                                          + np.mean(grid_force[ave:, 9])*1j)
                                self.fy_ave.append(np.mean(grid_force[ave:, 10]) 
                                                          + np.mean(grid_force[ave:, 11])*1j)
                                self.fz_ave.append(np.mean(grid_force[ave:, 12]) 
                                                          + np.mean(grid_force[ave:, 13])*1j)
                                self.mx_ave.append(np.mean(grid_force[ave:, 14]) 
                                                          + np.mean(grid_force[ave:, 15])*1j)
                                self.my_ave.append(np.mean(grid_force[ave:, 16]) 
                                                          + np.mean(grid_force[ave:, 17])*1j)
                                self.mz_ave.append(np.mean(grid_force[ave:, 18]) 
                                                          + np.mean(grid_force[ave:, 19])*1j)
                        except:
                            print('Error on reading of', filename)
                nlev += 1

        # read the grid.?F? files
        for xdir in ['X', 'Y', 'Z']:
            for grlev in range(1, 6):
                filename = self.project + '.' + str(grlev) + 'F' + xdir
                if os.path.exists(filename):
                    data = np.loadtxt(filename)
                    setattr(self, 'ext_%s_coords' % xdir, data[:, 0])
                    varx = getattr(self, 'ext_%s_fx' % xdir)
                    vary = getattr(self, 'ext_%s_fy' % xdir)
                    varz = getattr(self, 'ext_%s_fz' % xdir)
                    varxv = getattr(self, 'ext_%s_fxv' % xdir)
                    varyv = getattr(self, 'ext_%s_fyv' % xdir)
                    varzv = getattr(self, 'ext_%s_fzv' % xdir)
                    varx.append(data[:, 1] + data[:, 4])
                    vary.append(data[:, 2] + data[:, 5])
                    varz.append(data[:, 3] + data[:, 6])
                    varxv.append(data[:, 4])
                    varyv.append(data[:, 5])
                    varzv.append(data[:, 6])

        # read the grid.?P? files
        for xdir in ['X', 'Y', 'Z']:
            for grlev in range(1, 6):
                filename = self.project + '.' + str(grlev) + 'P' + xdir
                if os.path.exists(filename):
                    data = np.loadtxt(filename)
                    idx = []
                    for i in range(1000):
                        ix = np.where(data[:, 0] == float(i))[0]
                        if len(ix) > 0:
                            setattr(self, 'ext_%s_%iP_x%03d' % (xdir, grlev, i), data[ix, 1])
                            setattr(self, 'ext_%s_%iP_y%03d' % (xdir, grlev, i), data[ix, 2])
                            setattr(self, 'ext_%s_%iP_z%03d' % (xdir, grlev, i), data[ix, 3])
                            setattr(self, 'ext_%s_%iP%03d' % (xdir, grlev, i), data[ix, 4])
                            setattr(self, 'ext_%s_%iCF%03d' % (xdir, grlev, i), data[ix, 5])
                            setattr(self, 'ext_%s_%iP_ave%03d' % (xdir, grlev, i), data[ix, 6])
                            setattr(self, 'ext_%s_%iCF_ave%03d' % (xdir, grlev, i), data[ix, 7])
        
        # read the grid.*points file
        var_map = {}
        for grlev in range(1, 6):
            filename = self.project + '.%ipoints' % grlev
            if not os.path.exists(filename):
                break
            fid = open(filename, 'r')
            fid.readline()
            fid.readline()
            fid.readline()
            line = fid.readline()
            ivar = 0
            while line.startswith('#'):
                name = line.split()[-1]
                var_map[ivar] = name
                if not hasattr(self, 'pt_ext_%s' % name):
                    setattr(self, 'pt_ext_%s' % name, [])
                pos = fid.tell()
                line = fid.readline()
                ivar += 1
            # unroll readline so all data is read by loadtxt
            fid.seek(pos, 0)
            data = np.loadtxt(fid)
            for j in range(data.shape[1]):
                var = getattr(self, 'pt_ext_%s' % var_map[j])
                var.append(data[:, j])
            fid.close()
       
        # read the grid.*les file
        var_map = {}
        for grlev in range(1, 6):
            filename = self.project + '.%iles' % grlev
            if not os.path.exists(filename):
                break
            fid = open(filename, 'r')
            fid.readline()
            fid.readline()
            fid.readline()
            line = fid.readline()
            ivar = 0
            while line.startswith('#'):
                name = line.split()[-1]
                var_map[ivar] = name
                if not hasattr(self, 'les_ext_%s' % name):
                    setattr(self, 'les_ext_%s' % name, [])
                pos = fid.tell()
                line = fid.readline()
                ivar += 1
            # unroll readline so all data is read by loadtxt
            fid.seek(pos, 0)
            data = np.loadtxt(fid)
            for j in range(data.shape[1]):
                var = getattr(self, 'les_ext_%s' % var_map[j])
                var.append(data[:, j])
            fid.close()

        # read the grid.*ha file
        var_map = {}
        for grlev in range(1, 6):
            filename = self.project + '.%iha' % grlev
            if not os.path.exists(filename):
                break
            fid = open(filename, 'r')
            fid.readline()
            fid.readline()
            fid.readline()
            line = fid.readline()
            ivar = 0
            while line.startswith('#'):
                name = line.split()[-1]
                var_map[ivar] = name
                if not hasattr(self, 'ha_ext_%s' % name):
                    setattr(self, 'ha_ext_%s' % name, [])
                pos = fid.tell()
                line = fid.readline()
                ivar += 1
            # unroll readline so all data is read by loadtxt
            fid.seek(pos, 0)
            data = np.loadtxt(fid)
            for j in range(data.shape[1]):
                var = getattr(self, 'ha_ext_%s' % var_map[j])
                var.append(data[:, j])
            fid.close()

