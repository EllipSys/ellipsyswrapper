
import os,sys
import platform
import traceback
import glob
import numpy as np
import subprocess
import time
import shutil
import unittest
#import unittest2 as unittest
import pkg_resources
from ellipsyswrapper.ellipsys_output import EllipSys1DOutput,EllipSys2DOutput, EllipSys3DOutput
from ellipsyswrapper.ellipsys_input import read_ellipsys_input


class CaseRunner(object):

    def __init__(self, kwargs):

        self.project = 'grid'
        self.inputfile = 'input.dat'
        self.solver = 'ellipsys2d'
        self.inputs = []
        self.jobid = 0
        self.directory = ''
        self.nprocs = 1
        self.exec_mode = 'shell'
        self.runbatch = 'RunBatch1.1'
        self.preproc = 'basis2d'
        self.flowfield = 'flowfieldMPI'
        self.postproc = 'postprocessor'
        self.variables = []
        self.casename = 'testcase'
        self.reg_data = None
        self.tol = 6
        self.debug = False
        self.nstep_ave = 100
        self.auxilaryfiles = []
        self.keep_directory = False
        self.test_restart = False
        self.make_test_data_branch = None
        self.touch_dry_run = False
        self.cs_mode = None

        self.vars = []

        self.root_dir = os.getcwd()

        for k, w in kwargs.items():
            if hasattr(self, k):
                setattr(self, k, w)

        # check if USE_TEST_DATA_BRANCH is defined and switch branch
        branch = os.environ.get('USE_TEST_DATA_BRANCH', None)
        if branch is not None:
            exists = subprocess.check_output('git -C %s branch -a --list %s' % (self.directory, 'origin/'+branch), shell=True)
            print('EXISTS', exists, flush=True)
            if exists is not None:
                print('#################################################################################', flush=True)
                print('!!!!!! switching to branch %s for case %s' % (branch, self.casename), flush=True) 
                print('#################################################################################', flush=True)
                status = subprocess.check_output('git -C %s checkout %s' % (self.directory, branch), shell=True)
                print(status, flush=True)
                time.sleep(2.)
            else:
                print('branch %s does NOT exist, using master')

        self.inputobj = read_ellipsys_input(os.path.join(self.directory, self.inputfile))
        for inp in self.inputs:
            self.inputobj.set_entry(sorted(inp)[0], sorted(inp.values())[0])

        if self.solver == 'ellipsys1d':
            self._dim = 1
            self.reg_data = EllipSys1DOutput(os.path.join(self.directory, self.project))
        elif self.solver == 'ellipsys2d':
            self._dim = 2
            self.reg_data = EllipSys2DOutput(os.path.join(self.directory, self.project))
        elif self.solver == 'ellipsys3d':
            self._dim = 3
            self.reg_data = EllipSys3DOutput(os.path.join(self.directory, self.project))
        else:
            raise RuntimeError('solver must be either ellipsys2d or ellipsys3d, got %s' % self.solver)
        self.reg_data.nstep_ave = self.nstep_ave
        self.reg_data.read()

    def run_case(self):

        testroot = '/'.join(self.directory.split('/')[:-1])
        test_dir = os.path.join(testroot, self.casename)
        self.testroot = testroot
        print('#################################################################################', flush=True)
        print('### Test case: '+self.casename, flush=True)
        print('#################################################################################', flush=True)
        if os.path.exists(test_dir):
            if not self.keep_directory:
                # raise RuntimeWarning('Found old run directory %s, and deleted it!' % test_dir)
                print('Found old run directory %s, and deleted it!' % test_dir, flush=True)
                shutil.rmtree(test_dir)
                os.mkdir(test_dir)
            else:
                print('Found old run directory %s, keeping it!' % test_dir, flush=True)
        else:
            os.mkdir(test_dir)


        os.chdir(test_dir)
        # link grid files and write input.dat
        status = subprocess.check_output('ln -s ../grid/grid.*%iD .' % self._dim, shell=True)
        fid = open('input.dat', 'w')
        fid.write(self.inputobj._print())
        fid.close()
        # link additional files
        if self.auxilaryfiles:
            for filename in self.auxilaryfiles:
                status = subprocess.check_output('ln -s %s .' % os.path.join(self.root_dir, self.directory, filename), shell=True)
        sys.stdout.flush()
        os.system('echo -e "\e[0Ksection_start:`date +%s`:' + '%s[collapsed=true]\r\e[0K%s"' % (self.casename, self.casename + " test log"))
        self._run('output')
        os.system('echo -e "\e[0Ksection_end:`date +%s`:' + '%s\r\e[0K"' % self.casename)

        # check if we should push new test results to the repo
        self.make_test_data_branch = os.environ.get('MAKE_TEST_DATA_BRANCH', None) 
        if self.make_test_data_branch:
            exists = subprocess.check_output('git branch -a --list %s' % ('origin/'+self.make_test_data_branch), shell=True).decode()
            exists1 = subprocess.check_output('git branch -a --list %s' % (self.make_test_data_branch), shell=True).decode()
            print('================ generating new test data =================')
            if exists == '' and exists1 == '':
                print('creating branch %s' % self.make_test_data_branch, flush=True)
                subprocess.check_output('git checkout -b %s' % self.make_test_data_branch, shell=True)
            else:
                print('branch %s exists, switching to it' % self.make_test_data_branch, flush=True)
                subprocess.check_output('git fetch origin', shell=True)
                subprocess.check_output('git checkout %s' % (self.make_test_data_branch), shell=True)
            if os.environ.get('CI_JOB_ID', None) is not None:
                subprocess.check_output('git config user.email "%s"' % 'ellipsys_ci_bot@dtu.dk', shell=True)
                subprocess.check_output('git config user.name "%s"' % (os.environ.get('GITLAB_USER_NAME', 'Mr. Robot')), shell=True)
            testfiles = glob.glob(os.path.join(self.root_dir, self.directory, '*'))
            for name in testfiles:
                sname = name.split('/')[-1]
                if sname in ['input.dat']: continue
                print('Copying test results %s to %s' % (sname, os.path.join(self.root_dir, self.directory)), flush=True)
                if self.touch_dry_run:
                    print('modifying files for dry-run test', flush=True)
                    subprocess.check_output('echo "# dry-run touch" >> %s' % sname, shell=True)
                try:
                    shutil.copy(sname, os.path.join(self.root_dir, self.directory))
                except:
                    pass  # traceback.print_exc() 
                subprocess.check_output('git add %s' % (os.path.join(self.root_dir, self.directory, name)), shell=True)

            changes = subprocess.check_output('git status --untracked-files=no --porcelain', shell=True)
            if changes != '':
                commit_message = os.environ.get('CI_COMMIT_MESSAGE', 'updating test data for case %s' % self.casename)
                status = subprocess.check_output('git commit -m"%s"' % commit_message, shell=True)

                url = subprocess.check_output('git remote get-url origin', shell=True).decode().strip('\n')
                if os.environ.get('CI_JOB_ID', None) is not None:
                    bot_token = os.environ.get('BOT_ACCESS_TOKEN', None)
                    if bot_token is not None:
                        parts = url.split('//')
                        url2 = '%s//ellipsys_ci_bot:%s@%s' % (parts[0], bot_token, parts[1])
                        if "ellipsys_ci_bot" not in url: 
                            os.system('git remote set-url origin %s' % url2) 
                        command = 'git pull origin %s' % self.make_test_data_branch
                        print('checking remote branch %s for updates' % self.make_test_data_branch, flush=True)
                        try:
                            status = subprocess.check_output(command, shell=True)
                        except:
                            print("no updates")
                        command = 'git push origin %s' % self.make_test_data_branch
                        print('pushing code with ellipsys_ci_bot user', flush=True)
                        status = subprocess.check_output(command, shell=True)
                        print(status.decode(), flush=True) 
                    else:
                        print('BOT_ACCESS_TOKEN not defined, not pushing', flush=True)
                elif url[:4] == 'git@':
                    command = 'git push origin %s' % self.make_test_data_branch
                    print('pushing code with ssh access', flush=True)
                    status = subprocess.check_output(command, shell=True)
                    print(status.decode(), flush=True) 
                else:
                    print('Cannot push to remote, we have to either be in a CI context or remote has to be defined with ssh access', flush=True)
            else:
                print('No changes to test results for case %s' % self.casename, flush=True)

            os.chdir(self.root_dir)
            return
            

        if self.test_restart:
            print('### case '+self.casename+': test restart from coarse grid level')
            # remove output files
            files = ['grid.%iforce',
                     'grid.%ipoints',
                     'grid.%iHY',
                     'grid.%iFY',
                     'grid.%iPY',
                     'grid.%iles',
                     'grid.%iha']
            for i in range(1, 0, -1):
                for f in files:
                    try:
                        os.remove(f % i)
                        print('removed old output file', f % i)
                    except:
                        pass
            # read number of grid levels and mstep
            grlev = self.inputobj.get_entry('grid_level')
            mstep = self.inputobj.get_entry('mstep')
            msteporg=mstep[0]
            mstep[0]=int(mstep[0]/2)
            if grlev[0]>1:
                # after doing 0 iterations on grid level 2, we run half the 
                # number of iteration on the finest grlev before we restart again.
                subprocess.check_output('ln -s grid.RST.02 grid.rst', shell=True)
                mstep[1]=0
                # modify input file setting restart true
                self.inputobj.set_entry('restart', 'true')
                # if there is only 1 grid level, start at grid level 1 without restart, 
                # where we run half the number of iteration on the finest grlev before we restart again
            # Store initial restart file for later comparison.
            subprocess.check_output('mv grid.RST.01 grid.arst', shell=True)
            self.inputobj.set_entry('mstep', mstep)
            fid = open('input.dat', 'w')
            fid.write(self.inputobj._print())
            fid.close()
            sys.stdout.flush()
            os.system('echo -e "\e[0Ksection_start:`date +%s`:' + '%s[collapsed=true]\r\e[0K%s"' % (self.casename, self.casename + " restart1 test log"))
            self._run('output_rst')
            os.system('echo -e "\e[0Ksection_end:`date +%s`:' + '%s\r\e[0K"' % self.casename)
            # restart again on finer grlev with the other half of iterations.
            print('### case '+self.casename+': test restart at finest level from mstep =', mstep[0])
            if os.path.isfile('grid.rst'):
                subprocess.check_output('rm -rf grid.rst', shell=True)
            subprocess.check_output('ln -s grid.RST.01 grid.rst', shell=True)
            mstep[0]=msteporg-mstep[0] 
            self.inputobj.set_entry('mstep', mstep)
            self.inputobj.set_entry('restart', 'true')
            fid = open('input.dat', 'w')
            fid.write(self.inputobj._print())
            fid.close()
            sys.stdout.flush()
            os.system('echo -e "\e[0Ksection_start:`date +%s`:' + '%s[collapsed=true]\r\e[0K%s"' % (self.casename, self.casename + " restart2 test log"))
            self._run('output_rst')
            os.system('echo -e "\e[0Ksection_end:`date +%s`:' + '%s\r\e[0K"' % self.casename)
            # Compare restart files
            subprocess.check_output('ln -s grid.RST.01 grid.brst', shell=True)
            print('compare restart files')
            test = subprocess.call('cmp grid.arst grid.brst', shell=True)
            if(test==0):
                print('restart files are equal')
            elif(test==1):
                print('restart files are not equal!')
                print('compare fields')
                toolspath = os.getenv('TOOLS_PATH')
                subprocess.check_output(toolspath+'/CompareRST', shell=True)
                sys.exit()
            else:
                print('unknown output from cmp')
        # Clean up restart files
        subprocess.check_output('rm -rf *RST* *rst*', shell=True)
        os.chdir(self.root_dir)

    def _run(self, outname):

        host = platform.node()
        print("Host: %s" % host)
        if 'sophia' in host or 'sn' in host:
            mpirun = 'srun --mpi=pmix_v2 -n %i'
        else:
            mpirun = 'mpirun --allow-run-as-root -np %i'

        # get path to flowfieldMPI from an environment variable
        epath = os.getenv('ELLIPSYS%iD_PATH' % self._dim)
        if epath == '':
            raise RuntimeError('ELLIPSYS%iD_PATH env variable not set' % self._dim)

        if self.exec_mode == 'pbs':
            command = 'qsub RunBatch1.1'
            self.jobid = subprocess.call(command, shell=True)
            print('submitted case', self.directory, jobid)
        elif self.exec_mode == 'python':
            t0 = time.time()
            exepath = os.path.join(epath, self.flowfield)
            command = ' %s python %s' % (mpirun%self.nprocs, exepath)
            if self.debug:
              status = subprocess.call(command, shell=True)
            else:
              # Suppress output from EllipSys
              FNULL = open(os.devnull, 'w')
              status = subprocess.call(command, shell=True,stdout=FNULL)
            self.read_data(self.project, outname)
            self.jobid = None
            print('case %s ran in %f seconds' %(self.casename, time.time() - t0))
        elif self.exec_mode == 'shell':
            t0 = time.time()
            exepath = os.path.join(epath, self.flowfield)
            # serial computation
            if self.flowfield == 'flowfield':
                command = '%s' % (exepath)
            else:
                command = '%s %s' % (mpirun%self.nprocs, exepath)
            print('Executing command %s' % command, flush=True)
            # parallel computation
            if self.debug:
              status = subprocess.call(command, shell=True)
            else:
              # Suppress output from EllipSys
              FNULL = open(os.devnull, 'w')
              status = subprocess.call(command, shell=True,stdout=FNULL)
            if status != 0:
                raise RuntimeError('Case %s failed!' % self.casename)
            self.read_data(self.project, outname)
            self.jobid = None
            print('case %s ran in %f seconds' %(self.casename, time.time() - t0))
        elif self.exec_mode == 'dry_run':
            print('Running in dry run mode, copying pre-computed data!')
            files = glob.glob(os.path.join(self.root_dir, self.directory, '*'))
            for f in files:
                shutil.copy(f, '.')
            self.read_data(self.project, outname)
        else:
            raise RuntimeError('Unknown exec_mode: %s' % self.exec_mode)

    def clean(self):

        os.chdir(self.directory)
        files = glob.glob('*')

        delfiles = list(set(files)-set(self.files))
        for file in delfiles:
            if 'pkl' not in file:
                print('deleting file', file)
                os.remove(file)
        os.chdir('..')

    def read_data(self, project, output_obj='output'):

        if self.solver == 'ellipsys1d':
            setattr(self, output_obj, EllipSys1DOutput(project))
        elif self.solver == 'ellipsys2d':
            setattr(self, output_obj, EllipSys2DOutput(project))
        elif self.solver == 'ellipsys3d':
            setattr(self, output_obj, EllipSys3DOutput(project))
        else:
            raise RuntimeError('solver must be either ellipsys1d, ellipsys2d or ellipsys3d, got %s' % self.solver)
        out = getattr(self, output_obj)
        out.nstep_ave = self.nstep_ave
        out.read()

    def write_result(self):

        import Pickle

        os.chdir(self.directory)
        fid = open(self.casename+'.pkl','w')
        Pickle.dump(self.output, fid)
        fid.close()
        os.chdir('..')


class EllipSysTestCase(object):

    def test_ellipsys(self):

        if 'solver' in self.casedict.keys():
            self.solver = self.casedict['solver']
        else:
            self.solver = 'ellipsys2d'
            print('Warning: solver not set in casedict, assuming youre running EllipSys2D!')

        if 'exec_mode' in self.casedict and self.casedict['exec_mode'] == 'python':
            skip = False
            try:
                if self.solver == 'ellipsys2d':
                    if not pkg_resources.resource_exists('ellipsys2d', '_EllipSys2DWrapMPI.so'):
                        skip = True
                else:
                    if not pkg_resources.resource_exists('ellipsys3d', '_EllipSys3DWrapMPI.so'):
                        skip = True
            except:
                skip = True
            if skip:
               raise unittest.SkipTest('Python wrapped EllipSys2D not compiled')

        if 'flowfield' in self.casedict and self.casedict['flowfield'] == 'flowfield':
            if self.solver == 'ellipsys1d':
                if not os.path.exists(os.path.join(
                   os.getenv('ELLIPSYS1D_PATH'), 'flowfield')):
                   raise unittest.SkipTest('Serial EllipSys1D executable not compiled')
            elif self.solver == 'ellipsys2d':
                if not os.path.exists(os.path.join(
                   os.getenv('ELLIPSYS2D_PATH'), 'flowfield')):
                   raise unittest.SkipTest('Serial EllipSys2D executable not compiled')
            else:
                if not os.path.exists(os.path.join(
                   os.getenv('ELLIPSYS3D_PATH'), 'flowfield')):
                   raise unittest.SkipTest('Serial EllipSys3D executable not compiled')

        root_dir = os.getcwd()

        # skip test if test case directory is not present
        if not os.path.exists(os.path.join(root_dir,
                                           self.casedict['directory'])):
            raise unittest.SkipTest('Testcase directory not found')

        case = CaseRunner(self.casedict)
        if 'casename' not in self.casedict.keys():
            case.casename = self.__class__.__name__.split('.')[-1]
        case.run_case()

        if case.make_test_data_branch is not None:
            print('======= Skipping regression tests =============')
            return

        print('Checking results ...')
        self.__check_results(case, case.output)
        if case.test_restart:
            print('Checking rst results ...')
            self.__check_results(case, case.output_rst, suffix='_rst')
        os.chdir(root_dir)

    def __check_results(self, case, output, suffix=''):

        print('Test variables:', case.variables)
        if len(case.variables) != 0:
            for name in case.variables:
                with self.subTest(variable=name + suffix):
                    expected = getattr(case.reg_data, name)
                    if case.cs_mode == 'realpart':
                        result = [res.real for res in getattr(output, name)]
                    else:
                        result = getattr(output, name)
                    if isinstance(expected, list):
                        for i in range(len(expected)):
                            np.testing.assert_allclose(expected[i], result[i],
                                                       rtol=10**(-case.tol),
                                                       atol=10**(-case.tol))
                    elif isinstance(expected, float):
                        self.np.testing.assert_allclose(expected, result,
                                                        rtol=10**(-case.tol),
                                                        atol=10**(-case.tol))
        else:
            for name, expected in case.reg_data.__dict__.items():
                if case.cs_mode == 'realpart':
                    result = [res.real for res in getattr(output, name)]
                else:
                    result = getattr(output, name)
                with self.subTest(variable=name + suffix):
                    if isinstance(expected, list):
                        for i in range(len(expected)):
                            if isinstance(expected[i], (list, np.ndarray)):
                                np.testing.assert_allclose(expected[i], result[i],
                                                           rtol=10**(-case.tol),
                                                           atol=10**(-case.tol))
                            elif isinstance(expected[i], float):
                                np.testing.assert_allclose(expected[i], result[i],
                                                           rtol=10**(-case.tol),
                                                           atol=10**(-case.tol))
                    elif isinstance(expected, float):
                        np.testing.assert_allclose(expected, result,
                                                   rtol=10**(-case.tol),
                                                   atol=10**(-case.tol))
