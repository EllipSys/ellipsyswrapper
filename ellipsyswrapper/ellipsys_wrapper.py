
import numpy as np
import os
import time
import commands

from openmdao.core.component import Component

from PGL.main.curve import Curve
from ellipsyswrapper.ellipsys_output import EllipSys2DOutput, EllipSys3DOutput
from ellipsyswrapper.ellipsys_input import read_ellipsys_input

from ellipsys3d.EllipSys3DWrapMPI import e3d_start, \
                                         e3d_read, \
                                         e3d_outputpar, \
                                         e3d_initialize, \
                                         e3d_step, \
                                         pye3d_contr, \
                                         e3d_updatemeshfromfile, \
                                         bladeaxisintegralforces, \
                                         e3d_closefiles
from openmdao.core.mpi_wrap import MPI

if MPI:
    rank = MPI.COMM_WORLD.rank
else:
    rank = 0


class EllipSysRunner(object):

    def __init__(self, kwargs):

        self.project = 'grid'
        self.inputfile = 'input.dat'
        self.flowfield = 'flowfieldMPI'
        self.solver = 'ellipsys2d'
        self.inputs = []
        self.directory = ''
        self.nprocs = 1

        self.postproc = 'postprocessor'
        self.variables = []
        self.casename = 'testcase'
        self.debug = False
        self.nstep_ave = 100
        self.keep_directory = False
        self.restart_from_level = 0
        self.nstepmax = 500
        self.nstep0 = 1000
        self.nstep_out = 50
        self.write_restart = False
        self.rmax = 1.
        self.v0 = np.array([0., 1., 0.])

        self.vars = []

        self.root_dir = os.getcwd()

        for k, w in kwargs.iteritems():
            if hasattr(self, k):
                print 'setting attr', k, w
                setattr(self, k, w)

        self.inp = read_ellipsys_input(os.path.join(self.directory, self.inputfile))
        for inp in self.inputs:
            self.inp.set_entry(inp.keys()[0], inp.values()[0])

        if self.solver == 'ellipsys1d':
            self._dim = 1
            self.output = EllipSys1DOutput(self.project)
        elif self.solver == 'ellipsys2d':
            self._dim = 2
            self.output = EllipSys2DOutput(self.project)
        elif self.solver == 'ellipsys3d':
            self._dim = 3
            self.output = EllipSys3DOutput(self.project)

        self._called = False

    def execute(self):

        self.output.clear()
        self.output.nstep_ave = self.nstep_ave
        t0 = time.time()
        if rank == 0:
            print 'running ellipsys3d'
        if not self._called:
            self.inp.write('input.dat')
            e3d_start()
            e3d_outputpar()
        if not self._called:
            e3d_read()
        else:
            e3d_updatemeshfromfile()
        if not self._called:
            e3d_initialize()

        # intforces
        # fdx,fdy,fdz,fpx,fpy,fpz,
        # apx,apy,apz,anx,any,anz,
        # mdx,mdy,mdz,mpx,mpy,mpz
        # bladeforces
        # apx,apy,apz,anx,any,anz,
        # fdx,fdy,fdz,fpx,fpy,fpz,mx,my,mz)

        # try:
        self.int_forces = []
        bl_forces = []
        nstep = self.nstepmax
        if not self._called:
            nstep = self.nstep0 + self.nstepmax

        if self.write_restart:
            nstep -= 1
        for i in range(nstep):
            e3d_step()
            cforces = []
            for j in range(self.x.shape[0]):
                cforces.append(bladeaxisintegralforces(self.x[j], self.y[j], self.z[j],
                                                       self.nx[j], self.ny[j], self.nz[j], self.rmax, self.v0))
            bl_forces.append(np.asarray(cforces))
        if self.write_restart:
            os.system('touch grid.stop')
            e3d_step()
            cforces = []
            for j in range(self.x.shape[0]):
                cforces.append(bladeaxisintegralforces(self.x[j], self.y[j], self.z[j],
                                                       self.nx[j], self.ny[j], self.nz[j], self.rmax, self.v0))
            bl_forces.append(np.asarray(cforces))

        bl_forces = np.asarray(bl_forces)
        self.sec_forces = np.zeros(bl_forces[0, :, :].shape)
        ave = -min(bl_forces.shape[0], self.nstep_ave)
        for j in range(self.sec_forces.shape[1]):
            for i in range(self.sec_forces.shape[0]):
                self.sec_forces[i, j] = np.mean(bl_forces[ave:, i, j])

        if rank == 0:
            self.output.read()
        #except:
        #    if rank == 0: print 'EllipsysRunner crashed'

        self._called = True
        if rank == 0: print 'EllipSysRunner done!'


class EllipSysRunnerOpenMDAO(Component):

    def __init__(self, nprocs, axis_ni, config, suffix=''):
        super(EllipSysRunnerOpenMDAO, self).__init__()

        self.nprocs = nprocs

        self.e3d = EllipSysRunner(config)
        self.suffix = suffix
        # blade axis for bladeaxisintegralforces method
        self.add_param('x%s' % suffix, np.zeros(axis_ni))
        self.add_param('y%s' % suffix, np.zeros(axis_ni))
        self.add_param('z%s' % suffix, np.zeros(axis_ni))
        self.add_param('blade_length', 1.)

        # flags to catch crashed meshes
        self.add_param('surface_success', 1.)
        self.add_param('mesh_success', 1.)

        # flag to indicate crashed flow solution
        self.add_output('e3d_success', 1.)

        # integral force outputs
        self.add_output('fx_ave', 0.)
        self.add_output('fy_ave', 0.)
        self.add_output('fz_ave', 0.)
        self.add_output('mx_ave', 0.)
        self.add_output('my_ave', 0.)
        self.add_output('mz_ave', 0.)

        # extract y outputs
        if 'extract-y' in config['outputs'].keys():
            ni = config['outputs']['extract-y']
            self.add_output('ext_Y_coords', np.zeros(ni))
            self.add_output('ext_Y_fx', np.zeros(ni))
            self.add_output('ext_Y_fy', np.zeros(ni))
            self.add_output('ext_Y_fz', np.zeros(ni))

        # sectional forces from bladeaxisintegralforces
        self.add_output('sec_x', np.zeros(axis_ni))
        self.add_output('sec_y', np.zeros(axis_ni))
        self.add_output('sec_z', np.zeros(axis_ni))
        self.add_output('sec_s', np.zeros(axis_ni))
        self.add_output('sec_fx', np.zeros(axis_ni))
        self.add_output('sec_fy', np.zeros(axis_ni))
        self.add_output('sec_fz', np.zeros(axis_ni))
        self.add_output('sec_mx', np.zeros(axis_ni))
        self.add_output('sec_my', np.zeros(axis_ni))
        self.add_output('sec_mz', np.zeros(axis_ni))


    def solve_nonlinear(self, params, unknowns, resids):

        if params['surface_success'] == 1. and params['mesh_success'] == 1:


            axis = Curve(points=np.array([params['x%s' % self.suffix],
                                          params['y%s' % self.suffix],
                                          params['z%s' % self.suffix]]).T * params['blade_length'])
            # rotate into EllipSys coordinate system with blade along y+
            axis.rotate_x(-90.)
            axis.rotate_y(180.)
            try:

                self.e3d.x = axis.points[:, 0]
                self.e3d.y = axis.points[:, 1]
                self.e3d.z = axis.points[:, 2]
                self.e3d.nx = axis.dp[:, 0] * 0.    # x-component of normal set to zero so we slice in a y-z plane
                self.e3d.ny = axis.dp[:, 1]
                self.e3d.nz = axis.dp[:, 2]

                self.e3d.execute()

                if rank == 0:
                    if not np.isnan(self.e3d.output.fz[0]).any():
                        unknowns['e3d_success'] = 1.
                        for name in unknowns.keys():
                            try:
                                var = getattr(self.e3d.output, name)
                                if isinstance(var, list):
                                    unknowns[name] = var[0]
                                elif isinstance(var, np.ndarray):
                                    unknowns[name] = var
                                elif isinstance(var, float):
                                    unknowns[name] = var
                            except:
                                pass
                        unknowns['sec_s'] = axis.s * axis.smax
                        unknowns['sec_x'] = axis.points[:,0]
                        unknowns['sec_y'] = axis.points[:,1]
                        unknowns['sec_z'] = axis.points[:,2]
                        unknowns['sec_fx'] = self.e3d.sec_forces[:, 6] + self.e3d.sec_forces[:, 9]
                        unknowns['sec_fy'] = self.e3d.sec_forces[:, 7] + self.e3d.sec_forces[:,10]
                        unknowns['sec_fz'] = self.e3d.sec_forces[:, 8] + self.e3d.sec_forces[:,11]
                        unknowns['sec_mx'] = self.e3d.sec_forces[:,12]
                        unknowns['sec_my'] = self.e3d.sec_forces[:,13]
                        unknowns['sec_mz'] = self.e3d.sec_forces[:,14]

                        np.savetxt('binforce_mem.dat', np.array([unknowns['sec_s'],
                                                                 axis.points[:,0],
                                                                 axis.points[:,1],
                                                                 axis.points[:,2],
                                                                 unknowns['sec_fx'],
                                                                 unknowns['sec_fy'],
                                                                 unknowns['sec_fz'],
                                                                 unknowns['sec_mx'],
                                                                 unknowns['sec_my'],
                                                                 unknowns['sec_mz']]).T)
                    else:
                        unknowns['e3d_success'] = 0.

            except:

                unknowns['e3d_success'] = 0.
                print 'Ellipsys3D crashed'
        else:
            unknowns['e3d_success'] = 0.

    def get_req_procs(self):

        return (self.nprocs, self.nprocs)
