
import os
import shutil
import unittest
import pkg_resources
from ellipsyswrapper.testsuite import CaseRunner, EllipSysTestCase

PATH = pkg_resources.resource_filename('ellipsyswrapper', 'test')


def setup_caserunner():

    casedict = {'solver': 'ellipsys2d',
                'directory': os.path.join(PATH, 'outputs_data'),
                'inputfile': 'input.dat',
                'inputs': [{'mstep': [100, 100, 100]},
                           {'uinlet': 1.0}, {'vinlet': .0},
                           {'ufarfield': 1.0}, {'vfarfield': .0},
                           {'project': 'grid'}],
                'variables': ['fx_ave', 'fy_ave'],
                'exec_mode': 'dry_run',
                'casename': 'test2D',
                'nprocs': 4}
    c = CaseRunner(casedict)
    c.run_case()

    return c


class testEllipSysInput(unittest.TestCase):

    def setUp(self):

        pass

    def tearDown(self):

        try:
            shutil.rmtree(os.path.join(PATH, 'test2D'))
        except:
            pass

    def test_caserunner_dryrun(self):

        c = setup_caserunner()

        self.assertEqual(c.reg_data.fx_ave[0], c.output.fx_ave[0])

    def test_caserunner_dryrun(self):


        c = setup_caserunner()

        self.assertEqual(c.reg_data.fx_ave[0], c.output.fx_ave[0])


class test_EllipSysTestCase2D_with_vars(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver': 'ellipsys2d',
                         'directory': os.path.join(PATH, 'outputs_data'),
                         'inputfile': 'input.dat',
                         'inputs': [{'mstep': [100, 100, 100]},
                                    {'uinlet': 1.0}, {'vinlet': .0},
                                    {'ufarfield': 1.0}, {'vfarfield': .0},
                                    {'project': 'grid'}],
                         'variables': ['fx_ave', 'fy_ave'],
                         'exec_mode': 'dry_run',
                         'casename': 'test2D',
                         'nprocs': 4}

    def tearDown(self):

        try:
            shutil.rmtree(os.path.join(PATH, 'test2D'))
        except:
            pass


class test_EllipSysTestCase2D_all_vars(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver': 'ellipsys2d',
                         'directory': os.path.join(PATH, 'outputs_data'),
                         'inputfile': 'input.dat',
                         'inputs': [{'mstep': [100, 100, 100]},
                                    {'uinlet': 1.0}, {'vinlet': .0},
                                    {'ufarfield': 1.0}, {'vfarfield': .0},
                                    {'project': 'grid'}],
                         'exec_mode': 'dry_run',
                         'casename': 'test2D',
                         'nprocs': 4}

class test_EllipSysTestCase3D(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver': 'ellipsys3d',
                         'directory': os.path.join(PATH, 'outputs_data3d'),
                         'inputfile': 'input.dat',
                         'inputs': [{'mstep': [100, 100, 100]},
                                    {'uinlet': 1.0}, {'vinlet': .0},
                                    {'ufarfield': 1.0}, {'vfarfield': .0},
                                    {'project': 'grid'}],
                         'variables': ['fx_ave', 'fy_ave'],
                         'exec_mode': 'dry_run',
                         'casename': 'test3D',
                         'nprocs': 4}

    def tearDown(self):

        try:
            shutil.rmtree(os.path.join(PATH, 'test3D'))
        except:
            pass


class test_EllipSysTestCase3D_all_vars(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver': 'ellipsys3d',
                         'directory': os.path.join(PATH, 'outputs_data3d'),
                         'inputfile': 'input.dat',
                         'inputs': [{'mstep': [100, 100, 100]},
                                    {'uinlet': 1.0}, {'vinlet': .0},
                                    {'ufarfield': 1.0}, {'vfarfield': .0},
                                    {'project': 'grid'}],
                         'exec_mode': 'dry_run',
                         'casename': 'test3D',
                         'nprocs': 4}



    def tearDown(self):

        try:
            shutil.rmtree(os.path.join(PATH, 'test3D'))
        except:
            pass


class test_EllipSysTestCase3D_all_vars_rst(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver': 'ellipsys3d',
                         'directory': os.path.join(PATH, 'outputs_data3d'),
                         'inputfile': 'input.dat',
                         'inputs': [{'mstep': [100, 100, 100]},
                                    {'uinlet': 1.0}, {'vinlet': .0},
                                    {'ufarfield': 1.0}, {'vfarfield': .0},
                                    {'project': 'grid'}],
                         'restart_from_level': 3,
                         'exec_mode': 'dry_run',
                         'casename': 'test3D',
                         'nprocs': 4}



    def tearDown(self):

        try:
            shutil.rmtree(os.path.join(PATH, 'test3D'))
        except:
            pass

# class test_EllipSysTestCase3D_git_update(unittest.TestCase, EllipSysTestCase):
# 
#     def setUp(self):
# 
#         self.casedict = {'solver': 'ellipsys3d',
#                          'directory': os.path.join(PATH, 'outputs_data3d'),
#                          'inputfile': 'input.dat',
#                          'inputs': [{'mstep': [100, 100, 100]},
#                                     {'uinlet': 1.0}, {'vinlet': .0},
#                                     {'ufarfield': 1.0}, {'vfarfield': .0},
#                                     {'project': 'grid'}],
#                          'exec_mode': 'dry_run',
#                          'casename': 'test3D',
#                          'keep_directory': True,
#                          'nprocs': 4}
# 
#         os.environ['MAKE_TEST_DATA_BRANCH'] = 'test_case'
# 
# 
#     def tearDown(self):
# 
#         try:
#             shutil.rmtree(os.path.join(PATH, 'test3D'))
#         except:
#             pass
#         os.environ['MAKE_TEST_DATA_BRANCH'] = ''

if __name__ == '__main__':

    unittest.main()
