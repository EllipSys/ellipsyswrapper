
import os
import unittest
import pkg_resources
import numpy as np

from ellipsyswrapper.ellipsys_output import EllipSys2DOutput, EllipSys3DOutput

PATH = pkg_resources.resource_filename('ellipsyswrapper', 'test')

class testEllipSysInput(unittest.TestCase):

    def setUp(self):

        pass

    def tearDown(self):

        pass


    def test_read_output2D(self):

        o = EllipSys2DOutput(os.path.join(PATH, 'outputs_data/grid'))
        o.read()

        expected_fx = [np.array([ 0.00837095,  0.00886131,  0.00890271,  0.00890594,  0.0089094 ,
                               0.00891829,  0.00893148,  0.00894631,  0.00896141,  0.00897573]),
                    np.array([ 0.01240841,  0.012353  ,  0.01251796,  0.01246419,  0.01237713,
                               0.01229767,  0.01223061,  0.01217704,  0.01213776,  0.01210902]),
                    np.array([ 0.8748344 ,  0.07119878,  0.04822367,  0.02907621,  0.02218426,
                               0.02117174,  0.02137028,  0.02120496,  0.02065209,  0.01998315])]

        expected_fxave = [0.0088683536999999996, 0.012307278999999999, 0.11498995400000003]

        self.assertEqual(np.testing.assert_array_almost_equal(o.fx, expected_fx, decimal=5), None)

        self.assertEqual(np.testing.assert_array_almost_equal(o.fx_ave, expected_fxave, decimal=5), None)

    def test_read_output3D(self):

        o = EllipSys3DOutput(os.path.join(PATH, 'outputs_data3d/grid'))
        o.read()

        expected_fy = [np.array([ 0.        ,  0.20905524,  0.20905554,  0.20905569,  0.20905579,
                                  0.20905579,  0.20905569,  0.20905554,  0.20905524,  0.20901124]),
                       np.array([ 0.        ,  0.18991627,  0.18991379,  0.18991247,  0.18991207,
                                  0.18991207,  0.18991247,  0.18991379,  0.18991627,  0.18988575]),
                       np.array([ 0.        ,  0.19110632,  0.19111663,  0.19111811,  0.19111698,
                                  0.19111698,  0.19111811,  0.19111663,  0.19110631,  0.19107554])] 

        self.assertEqual(np.testing.assert_array_almost_equal(o.ext_Z_fy[0], expected_fy[0], decimal=5), None)


if __name__ == '__main__':

    unittest.main()
