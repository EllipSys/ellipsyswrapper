
import os
import unittest
import pkg_resources
from ellipsyswrapper.ellipsys_input import read_ellipsys_input

PATH = pkg_resources.resource_filename('ellipsyswrapper', 'test')

class testEllipSysInput(unittest.TestCase):

    def setUp(self):

        pass

    def tearDown(self):
        try:
            os.remove(os.path.join(PATH, 'tmp.inp'))
        except:
            pass

    def test_readwrite(self):

        i = read_ellipsys_input(os.path.join(PATH, 'dummy.inp'))

        fid = open(os.path.join(PATH, 'tmp.inp'), 'w')
        fid.write(i._print())
        fid.close()

        ii = read_ellipsys_input(os.path.join(PATH, 'tmp.inp'))

        self.assertEqual(i._print(), ii._print())

        val = i.get_entry('transtart')
        self.assertEqual(val, None)

    def test_set_entry(self):

        i = read_ellipsys_input(os.path.join(PATH, 'dummy.inp'))
        i.set_entry('mstep', [100, 100, 100])
        val = i.get_entry('mstep')

        self.assertEqual(val, [100, 100, 100])

    def test_set_new_entry(self):

        i = read_ellipsys_input(os.path.join(PATH, 'dummy.inp'))
        i.set_entry('restart', 'true')
        val = i.get_entry('restart')

        self.assertEqual(val, ['true'])

if __name__ == '__main__':

    unittest.main()
