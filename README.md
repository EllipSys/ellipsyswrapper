# ellipsyswrapper

`ellipsyswrapper` is a Python wrapper for the 2D and 3D Navier-Stokes flow solvers
EllipSys2D and EllipSys3D.

## License

EllipSys2D and EllipSys3D are a licensed software, and not included in this repository, 
and therefore needs to be installed separately.
To gain access to this code contact the main developer Niels N. Sørensen (nsqr@dtu.dk).

## Installation

To install the wrapper simply execute the command:

    $ python setup.py develop
    
To test that this module has been installed correctly
run the tests:

    $ nosetests .

from the repository root directory, or if you don't have `nose` installed:

    $ cd ellipsyswrapper/test
    $ python -m unittest discover '.' 'test_*.py'
